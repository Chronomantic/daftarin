class Major {
  int? id;
  String? name;
  String? desc;
  int? accepted;
  int? quota;

  Major({
    this.id,
    this.name,
    this.desc,
    this.accepted,
    this.quota,
  });

  Major.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    desc = json['desc'];
    accepted = json['accepted'];
    quota = json['quota'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['desc'] = this.desc;
    data['accepted'] = this.accepted;
    data['quota'] = this.quota;
    return data;
  }
}
