import 'package:daftar_sekolah/models/major.dart';

class Student {
  int? id;
  String? name;
  String? address;
  String? lastSchool;
  String? parent;
  String? status;
  num? phone;
  Major? major;

  Student({
    this.id,
    this.name,
    this.address,
    this.lastSchool,
    this.parent,
    this.status,
    this.phone,
    this.major,
  });

  Student.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address = json['address'];
    lastSchool = json['last_school'];
    parent = json['parent'];
    status = json['status'];
    phone = json['phone'];
    major = json['major'] != null ? Major.fromJson(json['major']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address'] = this.address;
    data['last_school'] = this.lastSchool;
    data['parent'] = this.parent;
    data['status'] = this.status;
    data['phone'] = this.phone;
    if (this.major != null) {
      data['major'] = this.major!.toJson();
    }
    return data;
  }
}
