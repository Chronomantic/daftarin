import 'package:daftar_sekolah/home_screen.dart';
import 'package:daftar_sekolah/screens/login/login_screen.dart';
import 'package:daftar_sekolah/screens/register/register_screen.dart';
import 'package:daftar_sekolah/screens/student/majors_screen.dart';
import 'package:daftar_sekolah/screens/student/status_registration_screen.dart';

import 'package:flutter/material.dart';

class RouteHelper {
  static const String kRouteHome = '/screens/home_screen';
  static const String kRouteLogin = '/screens/login/login_screen';
  static const String kRouteRegister = '/screens/register/register_screen';
  static const String kRouteMajors = '/screens/student/majors_screen';
  static const String kRouteStatusReg =
      '/screens/student/status_registration_screen';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case kRouteHome:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case kRouteLogin:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case kRouteRegister:
        return MaterialPageRoute(builder: (_) => RegisterScreen());
      case kRouteMajors:
        return MaterialPageRoute(builder: (_) => MajorsScreen());
      case kRouteStatusReg:
        return MaterialPageRoute(builder: (_) => StatusRegistrationScreen());
      default:
        return MaterialPageRoute(builder: (_) => LoginScreen());
    }
  }
}
