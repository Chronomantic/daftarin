import 'package:daftar_sekolah/screens/admin/cubit/majors_setting_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:daftar_sekolah/widgets/button_widget.dart';
import 'package:daftar_sekolah/widgets/text_field_widget/text_field_widget.dart';

class AddMajorScreen extends HookWidget {
  const AddMajorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => MajorsSettingCubit(),
        child: _AddMajorScreen(),
      ),
    );
  }
}

class _AddMajorScreen extends HookWidget {
  _AddMajorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _majorCubit = BlocProvider.of<MajorsSettingCubit>(context);

    final nameC = useTextEditingController();
    final descC = useTextEditingController();
    final quotaC = useTextEditingController();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: 38,
              width: 38,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffdbdbdb),
                    blurRadius: 8,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: Container(),
            ),
            IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              color: Color(0xFF707070),
              splashColor: Color(0xFFF5F5F5),
              highlightColor: Colors.transparent,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
        title: Text(
          'Jurusan Baru',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(15),
            child: Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffdbdbdb),
                    blurRadius: 8,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Data Jurusan',
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF696D6E),
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 15),
                  TextFieldWidget(
                    controller: nameC,
                    text: 'Nama Jurusan',
                    prefix: Icon(Icons.school_rounded, color: Colors.blue),
                  ),
                  SizedBox(height: 5),
                  TextFieldWidget(
                    controller: descC,
                    text: 'Deskripsi',
                    prefix: Icon(Icons.info, color: Colors.blue),
                    maxLines: 4,
                  ),
                  SizedBox(height: 5),
                  TextFieldWidget(
                    controller: quotaC,
                    text: 'Kuota Penerimaan',
                    prefix:
                        Icon(Icons.format_list_numbered, color: Colors.blue),
                    textInputType: TextInputType.phone,
                  ),
                  SizedBox(height: 5),
                  BlocConsumer<MajorsSettingCubit, MajorsSettingState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      return ButtonWidget(
                        text: 'Buat Baru',
                        onPressed: () {},
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
