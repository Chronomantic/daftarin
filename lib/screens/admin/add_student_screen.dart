import 'package:daftar_sekolah/screens/admin/cubit/student_candidate_cubit.dart';
import 'package:daftar_sekolah/widgets/circular_progress_indicator_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:daftar_sekolah/widgets/button_widget.dart';
import 'package:daftar_sekolah/widgets/text_field_widget/text_field_widget.dart';

class AddStudentScreen extends HookWidget {
  const AddStudentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => StudentCandidateCubit(),
        child: _AddStudentScreen(),
      ),
    );
  }
}

class _AddStudentScreen extends HookWidget {
  _AddStudentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _studentCubit = BlocProvider.of<StudentCandidateCubit>(context);

    final nameC = useTextEditingController();
    final addressC = useTextEditingController();
    final phoneC = useTextEditingController();
    final schoolC = useTextEditingController();
    final parentC = useTextEditingController();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: 38,
              width: 38,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffdbdbdb),
                    blurRadius: 8,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: Container(),
            ),
            IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              color: Color(0xFF707070),
              splashColor: Color(0xFFF5F5F5),
              highlightColor: Colors.transparent,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
        title: Text(
          'Daftarkan siswa',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(15),
            child: Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffdbdbdb),
                    blurRadius: 8,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Data diri',
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF696D6E),
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 15),
                  TextFieldWidget(
                    controller: nameC,
                    text: 'Nama',
                    prefix: Icon(Icons.person, color: Colors.blue),
                  ),
                  SizedBox(height: 5),
                  TextFieldWidget(
                    controller: addressC,
                    text: 'Alamat',
                    prefix: Icon(Icons.home_outlined, color: Colors.blue),
                    maxLines: 4,
                  ),
                  SizedBox(height: 5),
                  TextFieldWidget(
                    controller: phoneC,
                    text: 'Nomor Telepon',
                    prefix: Icon(Icons.phone, color: Colors.blue),
                    textInputType: TextInputType.phone,
                  ),
                  SizedBox(height: 5),
                  TextFieldWidget(
                    controller: schoolC,
                    text: 'Sekolah Asal',
                    prefix: Icon(Icons.school_outlined, color: Colors.blue),
                  ),
                  SizedBox(height: 5),
                  TextFieldWidget(
                    controller: parentC,
                    text: 'Nama Orang Tua',
                    prefix: Icon(Icons.emoji_people, color: Colors.blue),
                  ),
                  SizedBox(height: 5),
                  Container(
                    padding: EdgeInsets.only(left: 15, top: 10),
                    child: Row(
                      children: [
                        Text(
                          'Foto Siswa',
                          style: TextStyle(
                            fontSize: 16,
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                        SizedBox(width: 15),
                        InkWell(
                          onTap: () {
                            _showDialog(context, _studentCubit);
                          },
                          child: Icon(Icons.add_a_photo, color: Colors.black54),
                        ),
                      ],
                    ),
                  ),
                  _Picture(),
                  BlocConsumer<StudentCandidateCubit, StudentCandidateState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      return ButtonWidget(
                        text: 'Daftarkan',
                        onPressed: () {},
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _Picture extends HookWidget {
  _Picture({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    final _pickFile = BlocProvider.of<StudentCandidateCubit>(context);

    return Container(
      padding: const EdgeInsets.all(10),
      child: BlocConsumer<StudentCandidateCubit, StudentCandidateState>(
        listener: (context, state) {
          if (state is PickFileSuccess) {
            //showToastSuccess('success');
          }
        },
        builder: (context, state) {
          if (state is PickFileLoading) {
            return CircularProgressIndicatorWidget();
          }
          if (state is PickFileSuccess) {
            return InkWell(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return Container(
                      padding: EdgeInsets.all(20),
                      child: Image.file(
                        state.picture,
                      ),
                    );
                  },
                );
              },
              child: Container(
                height: 150,
                width: 150,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.file(
                    state.picture,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

void _showDialog(BuildContext context, StudentCandidateCubit _studentCubit) {
  showDialog(
    context: context,
    builder: (context) {
      return Dialog(
        child: Container(
          padding: EdgeInsets.all(15),
          height: 180,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Tambah Berkas',
                  style: TextStyle(fontSize: 16, color: Color(0xFF696D6E))),
              SizedBox(height: 10),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                  _studentCubit.takePicture();
                },
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffdbdbdb),
                        blurRadius: 8,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      Icon(Icons.add_a_photo, color: Colors.black54),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        color: Theme.of(context).dividerColor,
                        width: 0.5,
                        height: 15,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text('Dari kamera',
                          style:
                              TextStyle(fontSize: 16, color: Color(0xFF696D6E)))
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                  _studentCubit.pickFiles();
                },
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffdbdbdb),
                        blurRadius: 8,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      Icon(Icons.file_copy, color: Colors.black54),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        color: Theme.of(context).dividerColor,
                        width: 0.5,
                        height: 15,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text('Dari memori',
                          style:
                              TextStyle(fontSize: 16, color: Color(0xFF696D6E)))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
