import 'package:daftar_sekolah/screens/admin/add_major_screen.dart';
import 'package:daftar_sekolah/screens/admin/cubit/majors_setting_cubit.dart';
import 'package:daftar_sekolah/widgets/circular_progress_indicator_widget.dart';
import 'package:daftar_sekolah/widgets/drawer_admin.dart';
import 'package:daftar_sekolah/widgets/major_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class MajorsSettingScreen extends HookWidget {
  MajorsSettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => MajorsSettingCubit(),
        child: _MajorsSettingScreen(),
      ),
    );
  }
}

class _MajorsSettingScreen extends HookWidget {
  _MajorsSettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _majors = BlocProvider.of<MajorsSettingCubit>(context);

    useEffect(() {
      _majors.loadMajors();
      return;
    }, [_majors]);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: Builder(
          builder: (context) => InkWell(
            child: Icon(
              Icons.dehaze_rounded,
              color: Color(0xFF707070),
            ),
            onTap: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        title: Text('Daftar Jurusan', style: TextStyle(color: Colors.black)),
        actions: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 38,
                width: 38,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffdbdbdb),
                      blurRadius: 8,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Container(),
              ),
              IconButton(
                icon: Icon(Icons.add_circle_outline),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => AddMajorScreen(),
                    ),
                  );
                },
                color: Color(0xFF707070),
                splashColor: Color(0xFFF5F5F5),
                highlightColor: Colors.transparent,
              )
            ],
          ),
        ],
      ),
      endDrawerEnableOpenDragGesture: false,
      drawer: drawerAdmin(context),
      body: SingleChildScrollView(
        child: Container(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocConsumer<MajorsSettingCubit, MajorsSettingState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state is LoadMajorsSuccess) {
                      return Column(
                        children: <Widget>[
                          ListView.builder(
                            padding: EdgeInsets.only(top: 10),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: state.majors.length,
                            itemBuilder: (ctx, pos) {
                              return InkWell(
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: MajorWidget(major: state.majors[pos]),
                                ),
                                onTap: () {},
                              );
                            },
                          ),
                        ],
                      );
                    } else {
                      return Center(
                        child: CircularProgressIndicatorWidget(),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
