import 'package:daftar_sekolah/screens/admin/add_student_screen.dart';
import 'package:daftar_sekolah/screens/admin/cubit/student_candidate_cubit.dart';
import 'package:daftar_sekolah/screens/admin/student_profile_screen.dart';
import 'package:daftar_sekolah/widgets/circular_progress_indicator_widget.dart';
import 'package:daftar_sekolah/widgets/drawer_admin.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class StudentCandidateScreen extends HookWidget {
  StudentCandidateScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => StudentCandidateCubit(),
        child: _StudentCandidateScreen(),
      ),
    );
  }
}

class _StudentCandidateScreen extends HookWidget {
  _StudentCandidateScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _studentCubit = BlocProvider.of<StudentCandidateCubit>(context);

    useEffect(() {
      _studentCubit.loadStudents();
      return;
    }, [_studentCubit]);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: Builder(
          builder: (context) => InkWell(
            child: Icon(
              Icons.dehaze_rounded,
              color: Color(0xFF707070),
            ),
            onTap: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        title: Text('Calon Siswa', style: TextStyle(color: Colors.black)),
        actions: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 38,
                width: 38,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffdbdbdb),
                      blurRadius: 8,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Container(),
              ),
              IconButton(
                icon: Icon(Icons.add_circle_outline),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => AddStudentScreen(),
                    ),
                  );
                },
                color: Color(0xFF707070),
                splashColor: Color(0xFFF5F5F5),
                highlightColor: Colors.transparent,
              )
            ],
          ),
        ],
      ),
      endDrawerEnableOpenDragGesture: false,
      drawer: drawerAdmin(context),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocConsumer<StudentCandidateCubit, StudentCandidateState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state is LoadStudentsSuccess) {
                      return Column(
                        children: <Widget>[
                          ListView.builder(
                            padding: EdgeInsets.only(top: 10),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: state.listStudent.length,
                            itemBuilder: (ctx, pos) {
                              String _status = state.listStudent[pos].status!;
                              return InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  margin: EdgeInsets.only(bottom: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(24),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xffdbdbdb),
                                        blurRadius: 8,
                                        offset: Offset(
                                            0, 1), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              state.listStudent[pos].name!,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Color(0xFF696D6E),
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          Icon(
                                            Icons.circle,
                                            color: _status == 'Diterima'
                                                ? Colors.green
                                                : _status == 'Ditolak'
                                                    ? Colors.red
                                                    : _status == 'Ditinjau'
                                                        ? Colors.blue
                                                        : Colors.yellow,
                                            size: 10,
                                          ),
                                          SizedBox(width: 5),
                                          Text(_status),
                                        ],
                                      ),
                                      SizedBox(height: 5),
                                      Container(
                                        color: Theme.of(context).dividerColor,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height: 0.5,
                                      ),
                                      SizedBox(height: 5),
                                      state.listStudent[pos].major == null
                                          ? Text('-')
                                          : Text(state
                                              .listStudent[pos].major!.name!),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (_) => StudentProfileScreen(
                                              student: state.listStudent[pos],
                                            )),
                                  );
                                },
                              );
                            },
                          ),
                        ],
                      );
                    } else {
                      return CircularProgressIndicatorWidget();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
