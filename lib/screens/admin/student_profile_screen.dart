import 'package:daftar_sekolah/models/student.dart';
import 'package:daftar_sekolah/screens/admin/cubit/student_candidate_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class StudentProfileScreen extends HookWidget {
  const StudentProfileScreen({Key? key, required this.student})
      : super(key: key);

  final Student student;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => StudentCandidateCubit(),
        child: _StudentProfileScreen(
          student: student,
        ),
      ),
    );
  }
}

class _StudentProfileScreen extends HookWidget {
  _StudentProfileScreen({Key? key, required this.student}) : super(key: key);

  final Student student;

  @override
  Widget build(BuildContext context) {
    final _studentCubit = BlocProvider.of<StudentCandidateCubit>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: 38,
              width: 38,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffdbdbdb),
                    blurRadius: 8,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: Container(),
            ),
            IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded),
              color: Color(0xFF707070),
              splashColor: Color(0xFFF5F5F5),
              highlightColor: Colors.transparent,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
        actions: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 38,
                width: 38,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffdbdbdb),
                      blurRadius: 8,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Container(),
              ),
              IconButton(
                icon: Icon(Icons.edit),
                color: Color(0xFF707070),
                splashColor: Color(0xFFF5F5F5),
                highlightColor: Colors.transparent,
                onPressed: () {},
              ),
            ],
          ),
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 38,
                width: 38,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffdbdbdb),
                      blurRadius: 8,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Container(),
              ),
              IconButton(
                icon: Icon(Icons.delete),
                color: Color(0xFF707070),
                splashColor: Color(0xFFF5F5F5),
                highlightColor: Colors.transparent,
                onPressed: () {},
              ),
            ],
          ),
        ],
        title: Text(
          'Profil Siswa',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(15),
            child: Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffdbdbdb),
                    blurRadius: 8,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        height: 75,
                        width: 75,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Container(
                            color: Colors.blue,
                          ),
                        ),
                      ),
                      SizedBox(width: 15),
                      Text(
                        student.name!,
                        style: TextStyle(
                            fontSize: 20,
                            color: Color(0xFF696D6E),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Alamat:',
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF696D6E),
                        fontWeight: FontWeight.bold),
                  ),
                  Text(student.address!),
                  SizedBox(height: 10),
                  Text(
                    'No Telp:',
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF696D6E),
                        fontWeight: FontWeight.bold),
                  ),
                  Text('+62 ' + student.phone!.toString()),
                  SizedBox(height: 10),
                  Text(
                    'Sekolah Asal:',
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF696D6E),
                        fontWeight: FontWeight.bold),
                  ),
                  Text(student.lastSchool!),
                  SizedBox(height: 10),
                  Text(
                    'Nama Orang Tua:',
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF696D6E),
                        fontWeight: FontWeight.bold),
                  ),
                  Text(student.parent!),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
