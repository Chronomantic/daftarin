part of 'majors_setting_cubit.dart';

@immutable
abstract class MajorsSettingState {}

class MajorsSettingInitial extends MajorsSettingState {}

class LoadMajorsSuccess extends MajorsSettingState {
  final List<Major> majors;

  LoadMajorsSuccess(this.majors);
}
