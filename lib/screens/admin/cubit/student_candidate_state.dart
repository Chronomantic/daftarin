part of 'student_candidate_cubit.dart';

@immutable
abstract class StudentCandidateState {}

class StudentCandidateInitial extends StudentCandidateState {}

class PickFileLoading extends StudentCandidateState {}

class PickFileSuccess extends StudentCandidateState {
  final File picture;

  PickFileSuccess(this.picture);
}

class LoadStudentsSuccess extends StudentCandidateState {
  final List<Student> listStudent;

  LoadStudentsSuccess(this.listStudent);
}
