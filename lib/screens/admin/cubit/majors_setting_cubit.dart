import 'package:daftar_sekolah/models/major.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'majors_setting_state.dart';

class MajorsSettingCubit extends Cubit<MajorsSettingState> {
  MajorsSettingCubit() : super(MajorsSettingInitial());

  Future<String> _loadMajorAsset() async {
    return await rootBundle.loadString('assets/json_samples/major_list.json');
  }

  Future<void> loadMajors() async {
    String a = await _loadMajorAsset();
    Map<String, dynamic> userMap = jsonDecode(a);

    final List<Major> list =
        List.from(userMap['data']).map((e) => Major.fromJson(e)).toList();

    emit(LoadMajorsSuccess(list));
  }
}
