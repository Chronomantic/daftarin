import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:daftar_sekolah/models/student.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';

part 'student_candidate_state.dart';

class StudentCandidateCubit extends Cubit<StudentCandidateState> {
  StudentCandidateCubit() : super(StudentCandidateInitial());

  Future<String> _loadStudentAsset() async {
    return await rootBundle.loadString('assets/json_samples/students.json');
  }

  Future<void> loadStudents() async {
    String a = await _loadStudentAsset();
    Map<String, dynamic> userMap = jsonDecode(a);

    final List<Student> list =
        List.from(userMap['data']).map((e) => Student.fromJson(e)).toList();

    emit(LoadStudentsSuccess(list));
  }

  Future<void> pickFiles() async {
    File pickedFile;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    );
    // ignore: unnecessary_null_comparison
    if (result != null) {
      emit(PickFileLoading());
      pickedFile = File(result.files.single.path!);
      emit(PickFileSuccess(pickedFile));
    } else {
      print('No image selected.');
    }
  }

  Future<void> takePicture() async {
    final picker = ImagePicker();
    File pickedFile;

    final pickedPicture = await picker.getImage(source: ImageSource.camera);
    if (pickedPicture != null) {
      emit(PickFileLoading());
      pickedFile = File(pickedPicture.path);
      emit(PickFileSuccess(pickedFile));
    } else {
      print('No image selected.');
    }
  }
}
