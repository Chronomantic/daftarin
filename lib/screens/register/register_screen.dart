import 'package:daftar_sekolah/screens/login/login_screen.dart';
import 'package:daftar_sekolah/screens/register/complete_register_screen.dart';
import 'package:daftar_sekolah/screens/register/cubit/register_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:daftar_sekolah/widgets/button_widget.dart';
import 'package:daftar_sekolah/widgets/text_field_widget/text_field_widget.dart';

class RegisterScreen extends HookWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => RegisterCubit(),
        child: _RegisterScreen(),
      ),
    );
  }
}

class _RegisterScreen extends HookWidget {
  _RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _registerCubit = BlocProvider.of<RegisterCubit>(context);

    final emailC = useTextEditingController();
    final passC = useTextEditingController();
    final rePassC = useTextEditingController();

    bool _obscureText = true;

    return Container(
      height: double.infinity,
      child: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 130,
                  child: Image.asset(
                    'assets/logo_sample.png',
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(height: 15),
                TextFieldWidget(
                  controller: emailC,
                  text: 'Email',
                  prefix: Icon(Icons.alternate_email, color: Colors.blue),
                  textInputType: TextInputType.emailAddress,
                ),
                SizedBox(height: 5),
                TextFieldWidget(
                  controller: passC,
                  text: 'Password',
                  prefix: Icon(Icons.lock_outline, color: Colors.blue),
                  obscureText: _obscureText,
                ),
                SizedBox(height: 5),
                TextFieldWidget(
                  controller: rePassC,
                  text: 'Confirm Password',
                  prefix: Icon(Icons.lock_outline, color: Colors.blue),
                  obscureText: _obscureText,
                ),
                SizedBox(height: 10),
                BlocConsumer<RegisterCubit, RegisterState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    return ButtonWidget(
                      text: 'Lanjut',
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (_) => CompleteRegisterScreen(),
                          ),
                        );
                      },
                    );
                  },
                ),
                SizedBox(height: 5),
                Container(
                  color: Theme.of(context).dividerColor,
                  width: MediaQuery.of(context).size.width / 2,
                  height: 0.5,
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Sudah punya akun? '),
                    SizedBox(
                      width: 10,
                    ),
                    InkWell(
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.blue),
                      ),
                      onTap: () => Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (_) => LoginScreen())),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
