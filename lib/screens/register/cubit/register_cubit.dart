import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(RegisterInitial());

  Future<void> pickFiles() async {
    File pickedFile;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    );
    // ignore: unnecessary_null_comparison
    if (result != null) {
      emit(PickFileLoading());
      pickedFile = File(result.files.single.path!);
      emit(PickFileSuccess(pickedFile));
    } else {
      print('No image selected.');
    }
  }

  Future<void> takePicture() async {
    final picker = ImagePicker();
    File pickedFile;

    final pickedPicture = await picker.getImage(source: ImageSource.camera);
    if (pickedPicture != null) {
      emit(PickFileLoading());
      pickedFile = File(pickedPicture.path);
      emit(PickFileSuccess(pickedFile));
    } else {
      print('No image selected.');
    }
  }
}
