part of 'register_cubit.dart';

@immutable
abstract class RegisterState {}

class RegisterInitial extends RegisterState {}

class PickFileLoading extends RegisterState {}

class PickFileSuccess extends RegisterState {
  final File picture;

  PickFileSuccess(this.picture);
}
