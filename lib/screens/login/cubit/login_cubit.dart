import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginInitial());

  void validate({
    required String email,
    required String password,
  }) {
    bool isMailError;
    bool isPassError;

    //to Validate Email
    //only some wellknown mail provider, only allow '.' '_' '-', and to lowerCase
    if (email.contains('@')) {
      int a = email.lastIndexOf('@');
      String leftS = email.substring(0, a).toLowerCase();
      if (leftS.contains(RegExp(r"^[a-zA-Z0-9._-]*$"))) {
        if (email.contains('@gmail.com') ||
            email.contains('@yahoo.com') ||
            email.contains('@outlook.com') ||
            email.contains('@hotmail.com') ||
            email.contains('@widya.ai')) {
          isMailError = false;
        } else {
          isMailError = true;
        }
      } else {
        isMailError = true;
      }
    } else {
      isMailError = true;
    }

    //to Validate Password
    //must have at least 1 number and 1 text, with min 8 character
    if (password.contains(RegExp(r'^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$'))) {
      isPassError = false;
    } else {
      isPassError = true;
    }

    emit(Validate(isMailError, isPassError));
  }
}
