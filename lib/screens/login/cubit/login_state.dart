part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class Validate extends LoginState {
  final bool isMailError;
  final bool isPassError;

  Validate(this.isMailError, this.isPassError);
}
