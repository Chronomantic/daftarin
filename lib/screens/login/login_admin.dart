import 'package:daftar_sekolah/screens/admin/majors_setting_screen.dart';
import 'package:daftar_sekolah/screens/login/cubit/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:daftar_sekolah/widgets/button_widget.dart';
import 'package:daftar_sekolah/widgets/text_field_widget/text_field_widget.dart';

class LoginAdminScreen extends HookWidget {
  const LoginAdminScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocProvider(
        create: (context) => LoginCubit(),
        child: _LoginAdminScreen(),
      ),
    );
  }
}

class _LoginAdminScreen extends HookWidget {
  _LoginAdminScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _loginCubit = BlocProvider.of<LoginCubit>(context);

    final emailC = useTextEditingController();
    final passC = useTextEditingController();

    bool _obscureText = true;

    return Container(
      height: double.infinity,
      child: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 130,
                  child: Image.asset(
                    'assets/logo_sample.png',
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(height: 15),
                TextFieldWidget(
                  controller: emailC,
                  text: 'Email',
                  prefix: Icon(Icons.alternate_email, color: Colors.blue),
                  textInputType: TextInputType.emailAddress,
                ),
                SizedBox(height: 5),
                TextFieldWidget(
                  controller: passC,
                  text: 'Password',
                  prefix: Icon(Icons.lock_outline, color: Colors.blue),
                  obscureText: _obscureText,
                ),
                SizedBox(height: 10),
                BlocConsumer<LoginCubit, LoginState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    return ButtonWidget(
                      text: 'Login',
                      onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (_) => MajorsSettingScreen()),
                          (r) => false),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
