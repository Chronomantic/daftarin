import 'package:daftar_sekolah/screens/student/cubit/majors_cubit.dart';
import 'package:daftar_sekolah/screens/student/status_registration_screen.dart';
import 'package:daftar_sekolah/widgets/circular_progress_indicator_widget.dart';
import 'package:daftar_sekolah/widgets/drawer.dart';
import 'package:daftar_sekolah/widgets/major_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class MajorsScreen extends HookWidget {
  const MajorsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => MajorsCubit(),
        child: _MajorsScreen(),
      ),
    );
  }
}

class _MajorsScreen extends HookWidget {
  _MajorsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _majorsCubit = BlocProvider.of<MajorsCubit>(context);

    useEffect(() {
      _majorsCubit.loadMajors();
      return;
    }, [_majorsCubit]);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: Builder(
          builder: (context) => InkWell(
            child: Icon(
              Icons.dehaze_rounded,
              color: Color(0xFF707070),
            ),
            onTap: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        title: Text(
          'Pilihan Jurusan',
          style: TextStyle(color: Colors.black),
        ),
      ),
      endDrawerEnableOpenDragGesture: false,
      drawer: drawer(context),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFFF5F5F5),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(11),
                    bottomRight: Radius.circular(11),
                  ),
                ),
                height: 20,
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  children: [
                    BlocConsumer<MajorsCubit, MajorsState>(
                      listener: (context, state) {},
                      builder: (context, state) {
                        if (state is LoadMajorsSuccess) {
                          return Column(
                            children: <Widget>[
                              ListView.builder(
                                padding: EdgeInsets.only(top: 10),
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: state.majors.length,
                                itemBuilder: (ctx, pos) {
                                  return InkWell(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 10),
                                      child: MajorWidget(
                                        major: state.majors[pos],
                                      ),
                                    ),
                                    onTap: () {
                                      showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Dialog(
                                            child: Container(
                                              padding: EdgeInsets.all(15),
                                              height: 155,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                      'Mendaftar Teknologi dan Rekayasa?'),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                      Navigator.of(context)
                                                          .push(
                                                        MaterialPageRoute(
                                                          builder: (_) =>
                                                              StatusRegistrationScreen(),
                                                        ),
                                                      );
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(10),
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Color(
                                                                0xffdbdbdb),
                                                            blurRadius: 8,
                                                            offset: Offset(0,
                                                                1), // changes position of shadow
                                                          ),
                                                        ],
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          Icon(
                                                              Icons
                                                                  .check_circle_outline,
                                                              color: Colors
                                                                  .black54),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Container(
                                                            color: Theme.of(
                                                                    context)
                                                                .dividerColor,
                                                            width: 0.5,
                                                            height: 15,
                                                          ),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Text('Ya',
                                                              style: TextStyle(
                                                                  fontSize: 16,
                                                                  color: Color(
                                                                      0xFF696D6E)))
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(height: 10),
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(10),
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Color(
                                                                0xffdbdbdb),
                                                            blurRadius: 8,
                                                            offset: Offset(0,
                                                                1), // changes position of shadow
                                                          ),
                                                        ],
                                                      ),
                                                      child: Row(
                                                        children: [
                                                          Icon(Icons.dangerous,
                                                              color: Colors
                                                                  .black54),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Container(
                                                            color: Theme.of(
                                                                    context)
                                                                .dividerColor,
                                                            width: 0.5,
                                                            height: 15,
                                                          ),
                                                          SizedBox(
                                                            width: 10,
                                                          ),
                                                          Text(
                                                            'Tidak',
                                                            style: TextStyle(
                                                              fontSize: 16,
                                                              color: Color(
                                                                  0xFF696D6E),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  );
                                },
                              ),
                            ],
                          );
                        } else {
                          return CircularProgressIndicatorWidget();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
