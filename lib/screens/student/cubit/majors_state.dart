part of 'majors_cubit.dart';

@immutable
abstract class MajorsState {}

class MajorsInitial extends MajorsState {}

class LoadMajorsSuccess extends MajorsState {
  final List<Major> majors;

  LoadMajorsSuccess(this.majors);
}

class MajorsLoadLoading extends MajorsState {}

class MajorsLoadFailed extends MajorsState {}
