import 'package:daftar_sekolah/widgets/drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class StatusRegistrationScreen extends HookWidget {
  const StatusRegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _StatusRegistrationScreen(),
    );
  }
}

class _StatusRegistrationScreen extends HookWidget {
  _StatusRegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: Builder(
          builder: (context) => InkWell(
            child: Icon(
              Icons.dehaze_rounded,
              color: Color(0xFF707070),
            ),
            onTap: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        title: Text(
          'Status Pendaftaran',
          style: TextStyle(color: Colors.black),
        ),
      ),
      endDrawerEnableOpenDragGesture: false,
      drawer: drawer(context),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(24),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xffdbdbdb),
                    blurRadius: 8,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Text(
                    'Teknologi dan Rekayasa',
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF696D6E),
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 5),
                  Container(
                    color: Theme.of(context).dividerColor,
                    width: MediaQuery.of(context).size.width,
                    height: 0.5,
                  ),
                  SizedBox(height: 5),
                  Text(
                    'Teknologi dan Rekayasa adalah jurusan pengembangan yang dikhususkan untuk teknologi dan aplikasi terbaru',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF307D6F),
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          decoration: BoxDecoration(
                            color: Colors.yellow,
                            borderRadius: BorderRadius.circular(18),
                          ),
                          child: Row(
                            children: [
                              Text('Lolos'),
                              Stack(
                                alignment: Alignment.center,
                                children: [
                                  Icon(
                                    Icons.circle,
                                    color: Colors.lightGreen,
                                  ),
                                  Icon(
                                    Icons.check_rounded,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Expanded(child: SizedBox()),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
