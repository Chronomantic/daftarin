import 'package:daftar_sekolah/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: const Color(0xFF00A2E9), //top bar color
    statusBarIconBrightness: Brightness.light, //top bar icons
  ));

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  static final ColorScheme colorSchemeLight = ColorScheme.light(
    primary: Color(0xFF3D57FF),
    primaryVariant: const Color(0xFF64B6FF),
    secondary: const Color(0xFF696D6E),
    secondaryVariant: const Color(0xFFfb870c),
    surface: Colors.white,
    background: Colors.white,
    error: const Color(0xffb00020),
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.black87,
    brightness: Brightness.light,
  );

  final ThemeData themeDataLight = ThemeData(
    colorScheme: colorSchemeLight,
    primaryColor: colorSchemeLight.primary,
    accentColor: colorSchemeLight.secondary,
    buttonColor: colorSchemeLight.secondaryVariant,
    backgroundColor: colorSchemeLight.background,
    buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
    ),
    inputDecorationTheme: InputDecorationTheme(),
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Daftarin',
      theme: themeDataLight,
      home: HomeScreen(),
    );
  }
}
