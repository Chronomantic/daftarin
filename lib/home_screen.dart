import 'package:daftar_sekolah/screens/login/login_admin.dart';
import 'package:daftar_sekolah/screens/login/login_screen.dart';
import 'package:daftar_sekolah/screens/register/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:daftar_sekolah/widgets/button_widget.dart';

class HomeScreen extends HookWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _HomeScreen(),
    );
  }
}

class _HomeScreen extends HookWidget {
  _HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 130,
                child: Image.asset(
                  'assets/logo_sample.png',
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: 15),
              Container(
                color: Theme.of(context).dividerColor,
                width: MediaQuery.of(context).size.width / 2,
                height: 0.5,
              ),
              SizedBox(height: 15),
              ButtonWidget(
                text: 'Login',
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => LoginScreen()));
                },
              ),
              ButtonWidget(
                text: 'Register',
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => RegisterScreen()));
                },
              ),
              SizedBox(height: 5),
              Container(
                color: Theme.of(context).dividerColor,
                width: MediaQuery.of(context).size.width / 2,
                height: 0.5,
              ),
              SizedBox(height: 10),
              Text('Anda admin?'),
              InkWell(
                child: Text(
                  'login sebagai admin',
                  style: TextStyle(color: Colors.blue),
                ),
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => LoginAdminScreen(),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
