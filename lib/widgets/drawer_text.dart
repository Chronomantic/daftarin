import 'package:flutter/material.dart';

class DrawerText extends StatelessWidget {
  DrawerText({
    Key? key,
    required this.onTap,
    required this.text,
    required this.icon,
  }) : super(key: key);

  final String text;
  final IconData icon;

  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.only(bottom: 15),
        child: Row(
          children: [
            Icon(
              icon,
              color: Color(0xFF307D6F),
            ),
            SizedBox(width: 5),
            Container(
              color: Theme.of(context).dividerColor,
              width: 0.5,
              height: 22,
            ),
            SizedBox(width: 5),
            Text(
              '$text',
              style: TextStyle(
                fontSize: 16,
                color: Color(0xFF696D6E),
              ),
            )
          ],
        ),
      ),
    );
  }
}
