import 'package:flutter/material.dart';

class MessageViewWidget extends StatelessWidget {
  const MessageViewWidget({
    Key? key,
    this.msg = '',
    required this.onTap,
  }) : super(key: key);

  final String msg;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Material(
          child: InkWell(
            onTap: onTap,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(msg),
            ),
          ),
        ),
      ),
    );
  }
}
