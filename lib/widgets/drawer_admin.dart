import 'package:daftar_sekolah/home_screen.dart';
import 'package:daftar_sekolah/screens/admin/majors_setting_screen.dart';
import 'package:daftar_sekolah/screens/admin/student_candidate_screen.dart';
import 'package:daftar_sekolah/widgets/drawer_text.dart';
import 'package:flutter/material.dart';

Drawer drawerAdmin(BuildContext context) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    height: 65,
                    width: 100,
                    child: Image.asset(
                      'assets/logo_sample.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Daftarin',
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.black54),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Aplikasi Pendaftaran Jurusan Sekolah Menengah Kejuruan ABC',
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 14, color: Colors.black),
              )
            ],
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Color(0xFF00A2E9),
                Color(0xFF82F0E9),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              DrawerText(
                text: 'Jurusan',
                icon: Icons.dashboard_outlined,
                onTap: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (_) => MajorsSettingScreen()));
                },
              ),
              DrawerText(
                text: 'Calon Siswa',
                icon: Icons.people,
                onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (_) => StudentCandidateScreen()));
                },
              ),
              DrawerText(
                text: 'Keluar',
                icon: Icons.logout,
                onTap: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (_) => HomeScreen()),
                      (r) => false);
                  //logOut(context);
                },
              )
            ],
          ),
        )
      ],
    ),
  );
}
