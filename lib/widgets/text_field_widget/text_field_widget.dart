import 'package:daftar_sekolah/widgets/text_field_widget/cubit/text_field_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class TextFieldWidget extends HookWidget {
  TextFieldWidget({
    Key? key,
    required this.controller,
    required this.prefix,
    required this.text,
    this.obscureText = false,
    this.textInputType = TextInputType.text,
    this.maxLines = 1,
  }) : super(key: key);

  final TextEditingController controller;
  final Widget prefix;
  final String text;
  final bool obscureText;
  final TextInputType textInputType;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TextFieldCubit(),
      child: _TextFieldWidget(
        controller: controller,
        prefix: prefix,
        text: text,
        obscureText: obscureText,
        textInputType: textInputType,
        maxLines: maxLines,
      ),
    );
  }
}

class _TextFieldWidget extends HookWidget {
  _TextFieldWidget({
    Key? key,
    required this.controller,
    required this.prefix,
    required this.text,
    this.obscureText = false,
    required this.textInputType,
    required this.maxLines,
  }) : super(key: key);

  final TextEditingController controller;
  final Widget prefix;
  final String text;
  final bool obscureText;
  final TextInputType textInputType;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    final _textFieldCubit = BlocProvider.of<TextFieldCubit>(context);

    double _height;
    if (maxLines > 2) {
      _height = 90;
    } else {
      _height = 50;
    }

    bool _obscureText = obscureText;
    bool _isError = false;
    String _message = '';
    Icon _fieldVisibility =
        Icon(Icons.visibility_off_outlined, color: Colors.blue);

    return BlocConsumer<TextFieldCubit, TextFieldState>(
      listener: (context, state) {
        if (state is ChangeFieldVisibility) {
          _obscureText = state.visible;
          state.visible
              ? _fieldVisibility =
                  Icon(Icons.visibility_off_outlined, color: Colors.blue)
              : _fieldVisibility =
                  Icon(Icons.visibility_outlined, color: Colors.blue);
        }
        if (state is Validate) {
          _isError = state.isError;
          _message = state.messages;
        }
      },
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.blueAccent)),
              height: _height,
              child: Row(
                children: [
                  prefix,
                  SizedBox(width: 10),
                  Container(
                    color: Theme.of(context).dividerColor,
                    width: 0.5,
                    height: _height - 20,
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: TextField(
                      textAlignVertical: TextAlignVertical.center,
                      obscureText: _obscureText,
                      controller: controller,
                      keyboardType: textInputType,
                      decoration: InputDecoration.collapsed(
                        hintText: "$text",
                        border: InputBorder.none,
                      ),
                      maxLines: maxLines,
                    ),
                  ),
                  SizedBox(width: 10),
                  obscureText
                      ? InkWell(
                          onTap: () {
                            _textFieldCubit.changeFieldVisibility(
                                visible: !_obscureText);
                          },
                          child: _fieldVisibility)
                      : Container(),
                ],
              ),
            ),
            _isError
                ? Container(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      _message,
                      style: TextStyle(fontSize: 10, color: Colors.red),
                    ),
                  )
                : Container(),
          ],
        );
      },
    );
  }
}
