part of 'text_field_cubit.dart';

@immutable
abstract class TextFieldState {}

class TextFieldInitial extends TextFieldState {}

class ChangeFieldVisibility extends TextFieldState {
  final bool visible;

  ChangeFieldVisibility(this.visible);
}

class Validate extends TextFieldState {
  final bool isError;
  final String messages;

  Validate(this.isError, this.messages);
}
