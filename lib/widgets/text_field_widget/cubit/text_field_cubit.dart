import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'text_field_state.dart';

class TextFieldCubit extends Cubit<TextFieldState> {
  TextFieldCubit() : super(TextFieldInitial());

  Future<void> changeFieldVisibility({
    required bool visible,
  }) async {
    emit(ChangeFieldVisibility(visible));
  }
}
