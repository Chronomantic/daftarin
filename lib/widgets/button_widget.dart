import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class ButtonWidget extends HookWidget {
  ButtonWidget({
    required this.onPressed,
    Key? key,
    this.text = 'Button',
    this.fontSize = 20,
  }) : super(key: key);

  final String text;
  final double fontSize;

  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 150,
      margin: EdgeInsets.all(10),
      child: ElevatedButton(
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Theme.of(context).colorScheme.primary,
                  Theme.of(context).colorScheme.primaryVariant
                ],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            constraints: BoxConstraints(maxWidth: 250.0, minHeight: 50.0),
            alignment: Alignment.center,
            child: Text(
              "$text",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: fontSize),
            ),
          ),
        ),
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          primary: Theme.of(context).colorScheme.primary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(80),
          ),
          padding: EdgeInsets.all(0.0),
        ),
      ),
    );
  }
}
