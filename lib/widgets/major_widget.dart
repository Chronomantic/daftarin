import 'package:daftar_sekolah/models/major.dart';
import 'package:flutter/material.dart';

class MajorWidget extends StatelessWidget {
  MajorWidget({Key? key, required this.major}) : super(key: key);

  final Major major;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(24),
        boxShadow: [
          BoxShadow(
            color: Color(0xffdbdbdb),
            blurRadius: 8,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            major.name!,
            style: TextStyle(
                fontSize: 16,
                color: Color(0xFF696D6E),
                fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 5),
          Container(
            color: Theme.of(context).dividerColor,
            width: MediaQuery.of(context).size.width,
            height: 0.5,
          ),
          SizedBox(height: 5),
          Text(
            major.desc!,
            style: TextStyle(
              fontSize: 14,
              color: Color(0xFF307D6F),
            ),
          ),
          SizedBox(height: 15),
          Container(
            child: Row(
              children: [
                major.accepted! < major.quota!
                    ? Container(
                        padding: EdgeInsets.only(
                            top: 5, bottom: 5, left: 10, right: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.green),
                          borderRadius: BorderRadius.circular(18),
                        ),
                        child: Row(
                          children: [
                            Text('Pendaftaran dibuka'),
                            Icon(
                              Icons.circle,
                              size: 18,
                              color: Colors.green,
                            ),
                          ],
                        ))
                    : Container(
                        padding: EdgeInsets.only(
                            top: 5, bottom: 5, left: 10, right: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.red),
                          borderRadius: BorderRadius.circular(18),
                        ),
                        child: Row(
                          children: [
                            Text('Pendaftaran ditutup'),
                            Icon(
                              Icons.circle,
                              size: 18,
                              color: Colors.red,
                            ),
                          ],
                        ),
                      ),
                Expanded(child: SizedBox()),
                Text(
                    major.accepted!.toString() + '/' + major.quota!.toString()),
              ],
            ),
          )
        ],
      ),
    );
  }
}
